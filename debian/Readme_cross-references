     In his "Instructions for FOLDOC Guest Editors", the FOLDOC
editor, Denis Howe, says:

  You should mark up (as a cross reference) any term which one 
  might reasonably expect to find, whether or not a definition 
  currently exists. This allows me to tell which links people 
  are trying to follow and add definitions as required.

     The on-line description of the FOLDOC includes the following:

  Note that not all cross-references actually lead anywhere yet, but if
  you find one that leads to something inappropriate, please let me
  know.

     Therefore a cross-reference to a non-existent definition is not
an error, but is part of the editor's TODO list.  A bug report 
regarding such a hanging cross-reference doesn't warrant a severity 
greater than wishlist.

     Denis has a huge backlog of requested definitions - it is usually
at least a year before contributed definitions are inserted into the
FOLDOC, and much longer for requested definitions.  Therefore, when an
additional definition is requested, I usually prepare (preferably with
the help of the submitter), and submit proposed definitions for
wishlist items.  Usually, I add the proposed definition to Debian
releases pending their inclusion upstream.

